<?
add_action( 'woocommerce_single_product_summary', 'send_to_crm', 30 );
add_action( 'wp_ajax_init_transaction', 'init_transaction' );
register_activation_hook( __FILE__, 'crm_plugin_init' );
add_shortcode( 'current_user', 'relotis_current_user' );
add_shortcode('analysis', 'analysis');
add_shortcode('contacts', 'contacts');
add_shortcode('proposal', 'proposal');
add_shortcode('negotiation', 'negotiation');
add_shortcode('close', 'close');
add_shortcode('ulcrm', 'main_output');
add_shortcode('lteadmin', 'lteadmin');
add_action('wp_ajax_add_contact_to_db', 'add_contact_to_db');
add_action('wp_ajax_remove_contact_from_db', 'remove_contact_from_db');

add_action('wp_ajax_analysis_refresh_data', 'analysis_refresh_data');
add_action('wp_ajax_proposal_refresh_data', 'proposal_refresh_data');
add_action('wp_ajax_set_stage_as_complete', 'set_stage_as_complete');
add_action('wp_ajax_check_stage_as_complete', 'check_stage_as_complete');
add_action('wp_ajax_negotiation_refresh_data', 'negotiation_refresh_data');
add_action('wp_ajax_set_transaction_complete', 'set_transaction_complete');
add_action('wp_ajax_save_task', 'save_task');
add_action('wp_ajax_check_previous_stage_complete', 'check_previous_stage_complete');
add_action('wp_ajax_nopriv_check_previous_stage_complete', 'check_previous_stage_complete');
add_action( 'admin_menu', 'register_stat_page' );

add_action('manage_users_columns', 'register_custom_user_column');
add_action('manage_users_custom_column', 'register_custom_user_column_view', 10, 3);
add_action('wp_ajax_save_user_plan', 'save_user_plan');
add_action('wp_dashboard_setup', 'dashboard_view');
